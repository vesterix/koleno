using System;
using UnityEngine;

namespace UnityStandardAssets.Cameras
{
	public class FreeLookCam : MonoBehaviour
	{
		[SerializeField] private bool m_LockCursor = false;                   // Whether the cursor should be hidden and locked.

		[Range(0f, 10f)] [SerializeField] private float x_TurnSpeed = 1.5f;   // How fast the rig will rotate from user input.
		[Range(0f, 10f)] [SerializeField] private float y_TurnSpeed = 1.5f;   // How fast the rig will rotate from user input.
		[Range(0f, 5f)] [SerializeField] private float m_TurnSmoothing = 0.0f;                // How much smoothing to apply to the turn input, to reduce mouse-turn jerkiness


		private float m_LookAngle;                    // The rig's y axis rotation.
		private float m_TiltAngle;                    // The pivot's x axis rotation.

		private Quaternion m_TransformTargetRot;

		protected void Awake()
		{
			// Lock or unlock the cursor.
			Cursor.lockState = m_LockCursor ? CursorLockMode.Locked : CursorLockMode.None;
			Cursor.visible = !m_LockCursor;
			m_TransformTargetRot = transform.localRotation;
		}


		protected void Update()
		{
			HandleRotationMovement();
			if (m_LockCursor && Input.GetMouseButtonUp(0))
			{
				Cursor.lockState = m_LockCursor ? CursorLockMode.Locked : CursorLockMode.None;
				Cursor.visible = !m_LockCursor;
			}
		}


		private void OnDisable()
		{
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;
		}


		private void HandleRotationMovement()
		{
			if (Time.timeScale < float.Epsilon)
				return;

			// Read the user input
			var x = Input.GetAxis("Mouse X");
			var y = -Input.GetAxis("Mouse Y");

			// Adjust the look angle by an amount proportional to the turn speed and horizontal input.
			m_LookAngle += x * x_TurnSpeed;
			m_TiltAngle += y * y_TurnSpeed;

			// Rotate the rig (the root object) around Y axis only:
			m_TransformTargetRot = Quaternion.Euler(0f, m_LookAngle, 0f) * Quaternion.Euler(m_TiltAngle,0,0);

			
			if (m_TurnSmoothing > 0)
			{
				//m_Pivot.localRotation = Quaternion.Slerp(m_Pivot.localRotation, m_PivotTargetRot, m_TurnSmoothing * Time.deltaTime);
				transform.localRotation = Quaternion.Slerp(transform.localRotation, m_TransformTargetRot, m_TurnSmoothing * Time.deltaTime);
			}
			else
			{
				//m_Pivot.localRotation = m_PivotTargetRot;
				transform.localRotation = m_TransformTargetRot;
			}
		}
	}
}
