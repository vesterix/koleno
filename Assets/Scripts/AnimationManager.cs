﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AnimationManager : MonoBehaviour {

	public Animator currentAnim;
	public Anim animScr;
	public Slider bar;
	public Text degreeText;

	float degree=0f;
	float latsDegree = 0f;
	public static bool PlayAnim;
	public AnimationState st;
	public static float gradDelta=0.1f;
	public bool Endless=false;

	public Image playButton;
	bool  playToButton=false;

	void Start () {
		
		animScr = currentAnim.GetComponent<Anim> ();
		currentAnim.Play ("Take 001",0,0f);
	}


	
	void OnEnable()
	{
		StartPlayAnimation ();
	}
	void Update () {
		if (Input.GetKeyDown (KeyCode.O)) {
			currentAnim.SetFloat ("reverse", -1f);
		}
		if (Input.GetKeyDown (KeyCode.X)) {
			//currentAnim.Play ("Take 001",0,1.1f);
			Time.timeScale=0.01f;
			bar.value = 1.1f;
		}
	
		if (PlayAnim) {
			
			float normTime = currentAnim.GetCurrentAnimatorStateInfo (0).normalizedTime;
			if (normTime > 1f)
				normTime = 1f;
			if (normTime < 0f)
				normTime = 0f;
			degreeText.text = ((int)(normTime * 90f)).ToString ();

			bar.value = (normTime);

			
		
			latsDegree = normTime;
			if (Endless && (normTime >= 1f || normTime <= 0f)) {
				currentAnim.SetFloat ("reverse", -currentAnim.GetFloat ("reverse"));

			}

			if(!Endless)
			if (Mathf.Abs (currentAnim.GetCurrentAnimatorStateInfo (0).normalizedTime - degree) <= 0.005f) {
				currentAnim.Play ("Take 001",0,degree);
				degreeText.text = (Mathf.RoundToInt(degree * 90f)).ToString ();
				Debug.Log ("STOP");
				bar.value = degree;
				playToButton = false;
				currentAnim.speed = 0f;
				PlayAnim = false;
				latsDegree = degree;
			}
		}
	}

	public void SetAnimPos(float pos)
	{
		
		if (PlayAnim) {
			Debug.Log ("PLL");
			return;
		}
		StopPlayAnimation2 ();
		Debug.Log (pos);

		if (pos != -1f) {
			Debug.Log ("SET DEGREE="+pos.ToString());
			degree =  pos;
			playToButton = true;

		}
		else
			degree =  bar.value;
		if (degree < 0f)
			degree = 0f;
		if(degree>1f)
			degree=1f;
		currentAnim.SetFloat ("reverse", 1f);
		if (degree < latsDegree) {
			currentAnim.SetFloat ("reverse", -1f);
		}

		
		Debug.Log ("DEGREE+" + degree.ToString());
		Debug.Log (latsDegree);
		currentAnim.speed = 1f;
		PlayAnim = true;


		if(playToButton)
			currentAnim.Play ("Take 001",0,latsDegree);
		else
			currentAnim.Play ("Take 001",0,degree);

	}

	public 	void StartPlayAnimation()
	{
		Debug.Log (latsDegree);
		//currentAnim.Play ("Take 001",0,stopTime);
		currentAnim.speed = 1f;
		PlayAnim = true;
		Endless = true;
		SetButtonPlay ();


	}

	public void StopPlayAnimation2()
	{
		PlayAnim = false;
		Endless = false;

		currentAnim.speed = 0f;
	//	currentAnim.Play ("Take 001",0,stopTime);
		SetButtonStop ();
	}

	public void StopPlayAnimation()
	{
		PlayAnim = false;
		Endless = false;
	
		currentAnim.speed = 0f;
		//degreeText.text = ((int)((stopTime-(int)stopTime) * 90f)).ToString ();
		//bar.value = (stopTime - (int)stopTime) ;
		//currentAnim.Play ("Take 001",0,stopTime);
		SetButtonStop ();
	}

	public void PLA()
	{
		if (!Endless)
			StartPlayAnimation ();
		else
			StopPlayAnimation ();
	}

	void SetButtonStop()
	{
		playButton.sprite = (Sprite)Resources.Load ("btPlayBlue",typeof(Sprite)) as Sprite;
	}
	void SetButtonPlay()
	{
		playButton.sprite = (Sprite)Resources.Load ("btPauseBlue",typeof(Sprite)) as Sprite;
	}
}
