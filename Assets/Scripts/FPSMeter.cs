﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSMeter : MonoBehaviour {

	float fps;
	public Text countText;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		float delta = Time.deltaTime;
		fps = 1f / delta;
		countText.text = fps.ToString("000") + " fps";
	}
}
