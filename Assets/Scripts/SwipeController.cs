﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SwipeController : MonoBehaviour {





	public GameObject model;

	Vector2 scrolStart1;
	Vector2 scrolStart2;


	float scrolLength=0f;






	bool setTodef=false;
	public GameObject resetButton;
	public Renderer parentModelRenderer;
	public GameObject parentModel;


	float speedCoof=1f;



	public GameObject Settings;
	//SETTINGS
	float moveY=1.2f;
	public InputField moveYInp;
	float moveSpeed=1f;
	public InputField moveSpeedInp;
	float scrolMagnitude=20f;
	public InputField scrolmagnitudeInp;
	float moveX=0.9f;
	public InputField moveXInp;
	float rotCoof=25f;
	public InputField rotCoofInp;
	float scroolCoof=0.05f;
	public InputField scroolCoofInp;
	float deltaXRotate=1f;
	public InputField deltaXRotateInp;
	float deltaYRotate=1f;
	public InputField deltaYRotateInpInp;

	public InputField gradDeltaInp;
	/// 

	void Start () {
		
		speedCoof = (model.transform.position - Camera.main.transform.position).magnitude;
		LoadSettings ();
	}
	
	void LoadSettings()
	{
		moveYInp.text = moveY.ToString ();
		moveSpeedInp.text = moveSpeed.ToString ();
		scrolmagnitudeInp.text = scrolMagnitude.ToString ();
		moveXInp.text = moveX.ToString ();
		rotCoofInp.text = rotCoof.ToString ();
		scroolCoofInp.text = scroolCoof.ToString ();
		deltaXRotateInp.text = deltaXRotate.ToString ();
		deltaYRotateInpInp.text = deltaYRotate.ToString ();
		gradDeltaInp.text = AnimationManager.gradDelta.ToString ();
	}

	public void SetSettings()
	{
		moveY = (float)System.Convert.ToDouble (moveYInp.text);
		moveSpeed = (float)System.Convert.ToDouble (moveSpeedInp.text);
		scrolMagnitude = (float)System.Convert.ToDouble (scrolmagnitudeInp.text);
		moveX = (float)System.Convert.ToDouble (moveXInp.text);
		rotCoof = (float)System.Convert.ToDouble (rotCoofInp.text);
		scroolCoof = (float)System.Convert.ToDouble (scroolCoofInp.text);
		deltaXRotate = (float)System.Convert.ToDouble (deltaXRotateInp.text);
		deltaYRotate = (float)System.Convert.ToDouble (deltaYRotateInpInp.text);
		AnimationManager.gradDelta = (float)System.Convert.ToDouble (gradDeltaInp.text);
		Settings.SetActive (false);
	}

	public void ShowSettings()
	{
		Settings.SetActive (true);
	}
	void Update () {
		


		if (setTodef)
			return;
		
		if (Input.touchCount == 1 ) {
			if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject (Input.touches[0].fingerId)) {
				
				return;
			}
		


			if (Input.touches [0].phase == TouchPhase.Moved) {
				Vector2 oneV = Input.touches [0].deltaPosition;
				Vector2 r = new Vector2(0f,0f);
			/*	r.x = -oneV.y;
				r.y = oneV.x;
				if (Mathf.Abs (r.x) < deltaXRotate)
					r.x = 0f;
				if (Mathf.Abs (r.y) < deltaYRotate)
					r.y = 0f;
				if (r.y == 0 && r.x == 0)*/
				if (Mathf.Abs (oneV.x) > Mathf.Abs (oneV.y))
					r.y = oneV.x;
				else
					r.x = -oneV.y;
				resetButton.SetActive (true);
				RotateModel (r.normalized);
					
					
				}




		} else if (Input.touchCount == 2) {
			Debug.Log ("2 touch");

			if (Input.touches [0].phase == TouchPhase.Began && Input.touches [1].phase == TouchPhase.Began) {
				scrolStart1 = Input.touches [0].position;
			
				scrolStart2 = Input.touches [1].position;
			


				scrolLength = Mathf.Sqrt(Mathf.Pow((scrolStart1.x-scrolStart2.x),2) + Mathf.Pow((scrolStart1.y-scrolStart2.y),2));

			}

			if (Input.touches [0].phase == TouchPhase.Moved && Input.touches [1].phase == TouchPhase.Moved) {
				Vector3 mPos = model.transform.position;
			
				Vector2 v1 = Input.touches [0].deltaPosition;
				Vector2 v2 = Input.touches [1].deltaPosition;
				float dirAngle = Vector2.Dot (v1.normalized, v2.normalized);
					Debug.Log ("DOT=" +dirAngle.ToString ());
				if (dirAngle > 0.5f) {
					Vector3 move;
					if (v1.magnitude > v2.magnitude)
						move = v1;
					else
						move = v2;
					resetButton.SetActive (true);

					bool visible = model.GetComponent<Renderer> ().isVisible;
					float marginX = 5f * (Mathf.Abs ((model.transform.position.z - Camera.main.transform.position.z)) / 8f);
					float marginY = 5f * (Mathf.Abs ((model.transform.position.z - Camera.main.transform.position.z)) / 8f);
					Debug.Log (visible);
					if (!visible) {
						if (model.transform.position.x > marginX && move.x > 0f)
							move.x = 0f;
						if (model.transform.position.x < -marginX && move.x < 0f)
							move.x = 0f;
						if (model.transform.position.y > marginY && move.y > 0f)
							move.y = 0f;
						if (model.transform.position.y < -marginY && move.y < 0f)
							move.y = 0f;
					}
					//speedCoof = .magnitude;
					Debug.Log (marginX);
					Debug.Log (marginY);
					Debug.Log (Screen.height);
					move.x = move.x /(float) Screen.height*moveX*marginX;
					move.y = move.y /(float) Screen.width*moveY*marginY;
					//Debug.Log (move.x);

					model.transform.position += move ;
				} else {
					Debug.Log ("SCROL");
				
					scrolStart1 = Input.touches [0].position;
					scrolStart2 = Input.touches [1].position;
					speedCoof = (model.transform.position - Camera.main.transform.position).magnitude + ( Input.touches [0].deltaPosition.magnitude +  Input.touches [0].deltaPosition.magnitude)*scroolCoof;
					float scrolLengthTemp = Mathf.Sqrt(Mathf.Pow((scrolStart1.x-scrolStart2.x),2) + Mathf.Pow((scrolStart1.y-scrolStart2.y),2));
					resetButton.SetActive (true);
					if(scrolLengthTemp>scrolLength)
						ScroolUp();
					else
						ScroolDown();
					scrolLength = scrolLengthTemp;


				}

			}
			/*	if (Input.touches [0].phase == TouchPhase.Moved && Input.touches [1].phase == TouchPhase.Moved
				&& (Input.touches [0].position-scrolStart1).magnitude > scrolMagnitude && (Input.touches [1].position-scrolStart2).magnitude > scrolMagnitude 
				&& Input.touches [1].deltaPosition.magnitude>scrolMagnitudeDelta && Input.touches [0].deltaPosition.magnitude>scrolMagnitudeDelta ) {

				speedCoof = (model.transform.position - Camera.main.transform.position).magnitude;
						scrolStart1 = Input.touches [0].position;
						scrolStart2 = Input.touches [1].position;
						float scrolLengthTemp = Mathf.Sqrt(Mathf.Pow((scrolStart1.x-scrolStart2.x),2) + Mathf.Pow((scrolStart1.y-scrolStart2.y),2));
				resetButton.SetActive (true);
						if(scrolLengthTemp>scrolLength)
							ScroolUp();
						else
							ScroolDown();
						scrolLength = scrolLengthTemp;
				return;
			}
			if (Input.touches [1].phase == TouchPhase.Began) {
				scrolStart2 = Input.touches [1].position;
				touchNow=2;
				rotStart2 = Input.touches [1].position;
			}

			if (Input.touches [0].phase == TouchPhase.Stationary && Input.touches [1].phase == TouchPhase.Moved) {

				//Vector2 rot1 = Input.touches [0].position;
				Vector2 rot2 = Input.touches [1].position;
				float rotlLengthTempY =rotStart2.y-rot2.y;
				float rotlLengthTempX =rotStart2.x-rot2.x;
				Debug.Log ("LEngthY="+rotlLengthTempY.ToString());
				scrolStart1 = Input.touches [0].position;
				scrolStart2 = Input.touches [1].position;

				resetButton.SetActive (true);
				if ( Mathf.Abs( rotlLengthTempX) > Mathf.Abs(rotlLengthTempY)) {
					if (rotlLengthTempX>0)
						RotateR ();
					else {
						RotateL ();
					}
				} else {
					

					if (rotlLengthTempY>0)
						RotateUp ();
					else {
						RotateDown ();
					}
				}

			}

			if (Input.touches [0].phase == TouchPhase.Ended) {
				touchNow--;
			}
			if (Input.touches [1].phase == TouchPhase.Ended) {
				touchNow--;
			} }*/




			


		
	}
		if (Input.GetKey (KeyCode.W)) {
			RotateUp ();
		}
		if (Input.GetKey (KeyCode.S)) {
			RotateDown ();
		}
		if (Input.GetKey (KeyCode.D)) {
			RotateL ();
		}
		if (Input.GetKey (KeyCode.A)) {
			RotateR ();
		}

		if (Input.GetKeyDown (KeyCode.Space)) {
			StartCoroutine (RotateBack ());
		}
	}

	void ScroolUp()
	{
		model.transform.position -= Vector3.forward * Time.deltaTime*speedCoof;
	}
	void ScroolDown()
	{
		model.transform.position += Vector3.forward * Time.deltaTime*speedCoof;
	}

	void RotateL()
	{
		Quaternion q = model.transform.localRotation;
		q.eulerAngles += Vector3.up * Time.deltaTime * rotCoof *speedCoof; 
		model.transform.localRotation = q;
	}

	void RotateR()
	{
		Quaternion q = model.transform.localRotation;
		q.eulerAngles -= Vector3.up * Time.deltaTime * rotCoof *speedCoof; 
		model.transform.localRotation = q;
	}
	void RotateUp()
	{
		
		model.transform.Rotate (Vector3.right* rotCoof *speedCoof* Time.deltaTime, Space.World);
	}

	void RotateDown()
	{
		
		model.transform.Rotate (-Vector3.right* rotCoof *speedCoof* Time.deltaTime, Space.World);
	}

	void RotateModel(Vector3 dirV)
	{
		model.transform.Rotate (dirV* rotCoof *speedCoof* Time.deltaTime, Space.World);					
	}

	IEnumerator RotateBack()
	{
		
		setTodef = true;
		float step = 1f;
		Debug.Log (model.transform.rotation);
		Debug.Log (model.transform.rotation == new Quaternion (90f, 0f, 90f, 0f));
		float st1 = 0f;
		Quaternion q = model.transform.rotation;
		Quaternion q2 = new Quaternion (-0.5f, 0.5f, 0.5f, 0.5f);
		while (st1<1f) {
			st1 += step * Time.deltaTime;
			//model.transform.rotation = Quaternion.RotateTowards (model.transform.rotation, new Quaternion (0.5f, -0.5f, 0.5f, 0.5f), st1);
			model.transform.rotation = Quaternion.Slerp(q,q2,st1);
			Debug.Log (st1);
			yield return null;
		}
		Debug.Log (model.transform.rotation);
		StartCoroutine (PosBack());
	}
	IEnumerator PosBack()
	{

		setTodef = false;
		resetButton.SetActive (false);
		Vector3 tarPos = new Vector3 (0.0f, 0f, 0f);
		Vector3 startPos = model.transform.position;
		float step = 0f;

		while (model.transform.position != tarPos) {
			model.transform.position = Vector3.Lerp (startPos, tarPos, step);
		
			step += Time.deltaTime;
			yield return null;
		}
		resetButton.SetActive (false);
		setTodef = false;

	}

	public void ResetToDefault()
	{
		if (setTodef)
			return ;
		StartCoroutine (RotateBack ());
	}


}
