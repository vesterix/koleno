﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anim : MonoBehaviour {

	int stopPos=0;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void StopPlayTwo()
	{
		if (stopPos == 2) {
			GetComponent<Animator> ().speed = 0f;
			AnimationManager.PlayAnim = false;
		}
	}

	public void SetStopPos(int _stop)
	{
		stopPos = _stop;
	}
}
