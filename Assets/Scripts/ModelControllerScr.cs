﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModelControllerScr : MonoBehaviour {

	public GameObject modelParent;
	public GameObject modelParent2;

	public Button rightBt;

	public GameObject baseModel;
	public Button leftBt;

	int layerState=0;
	public AnimationManager animManager;


	public GameObject[] layers;

	public GameObject currentLayerIco;
	public GameObject[] layersActiveIco;
	public GameObject[] layersIco;
	void OnEnable () {


	



	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetModelLeft()
	{
		modelParent.transform.localScale = new Vector3(1,1,1);
		leftBt.interactable = false;
		rightBt.interactable = true;

	}

	public void SetModelRight()
	{
		modelParent.transform.localScale = new Vector3(1,-1,1);
		leftBt.interactable = true;
		rightBt.interactable = false;

	}

	public void addLayerState()
	{
		if (layerState >= 3)
			return;
		if (AnimationManager.PlayAnim)
			return;
		layersActiveIco [layerState].SetActive (true);
		layers [layerState].SetActive (true);
		layerState++;
		currentLayerIco.GetComponent<RectTransform> ().localPosition = layersIco [layerState].GetComponent<RectTransform> ().localPosition;

	}

	public void minusLayerState()
	{
		if (layerState <=0)
			return;
		if (AnimationManager.PlayAnim)
			return;
		
	
		layersActiveIco [layerState].SetActive (false);
		layerState--;
		layers [layerState].SetActive (false);
		currentLayerIco.GetComponent<RectTransform> ().localPosition = layersIco [layerState].GetComponent<RectTransform> ().localPosition;
	}

}
