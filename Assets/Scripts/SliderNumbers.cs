﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderNumbers : MonoBehaviour {

	public RectTransform tr;
	public RectTransform tr2;
	public RectTransform handle;
	public Slider sld;
	public GameObject buttonThr;

	void Start () {
		tr2.position = tr.position;
		handle.position = tr.position;
		sld.value = 0f;

	}
	
	// Update is called once per frame
	void Update () {

		if (sld.value > 0.30f && sld.value < 0.37f) {
			if (tr2.gameObject.activeSelf)
				tr2.gameObject.SetActive (false);
		} else if (!tr2.gameObject.activeSelf) {
			
			tr2.gameObject.SetActive (true);
		}

		
	}
}
